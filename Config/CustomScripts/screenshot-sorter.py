import os
import shutil
from datetime import datetime
from pathlib import Path

image_formats = {".webp", ".jfif",".act",".ase",".gpl",".pal",".icc",".icm",".art",".blp",".bmp",".bti",".cd5",".cit",".cr2",".clip",".dds",".dib",".djvu",".exif",".gif",".gifv",".grf",".iff",".jng",".jpg",".jpeg", ".jp2",".jps",".kra",".lbm",".max",".miff",".msp",".nitf",".otb",".pbm",".pc1",".pc2",".pc3",".pcx",".pdn",".pgm",".pi1",".pi2",".pi3",".pict",".pct",".png",".pnm",".pns",".ppm",".psb",".pdd",".psp",".px",".pxm",".pxr",".qfx",".raw",".rle",".sct",".sgi",".tga",".tiff",".vtf",".xbm",".xcf",".xpm",".zif",".3dv",".amf",".awg",".ai",".cgm",".cdr",".cmx",".dp",".dxf",".e2d",".fs",".gbr",".odg",".svg",".stl",".vrml",".x3d",".sxd",".tgax",".v2d",".vdoc",".vsd",".vsdx",".vnd",".wmf",".emf",".xar"}

def sort_screenshots():
    screenshots_dir = Path.home() / "Pictures" / "Screenshots"
    if not screenshots_dir.exists():
        print(f"Directory {screenshots_dir} does not exist.")
        return
    
    for file in screenshots_dir.iterdir():
        if file.is_file() and file.suffix.lower() in image_formats:
            timestamp = datetime.fromtimestamp(file.stat().st_mtime)
            year = timestamp.strftime("%Y")
            month = timestamp.strftime("%m")
            new_name = timestamp.strftime("%d-%H-%M-%S") + file.suffix
            
            target_dir = screenshots_dir / year / month
            target_dir.mkdir(parents=True, exist_ok=True)
            
            target_path = target_dir / new_name
            shutil.move(file, target_path)
            print(f"Moved {file.name} -> {target_path}")

if __name__ == "__main__":
    sort_screenshots()

