#!/bin/bash

# Get the ID of the focused window
focused_window=$(bspc query -N -n focused)

# Get the state of the focused window (tiled or floating)
state=$(bspc query -T -n | grep -o "\"state\":\"floating\"")
echo $state

if [ "$state" == "" ]; then
  # If the current state is tiled, set window to floating
  bspc node $focused_window -t floating
else
  # If the current state is floating, set windows to tiled
  bspc node $focused_window -t tiled
fi
