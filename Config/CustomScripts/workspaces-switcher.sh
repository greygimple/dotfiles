#! /bin/bash

#options to be displayed
option01="Monitor 1 - Workspace 1"
option11="Monitor 1 - Workspace 2"
option21="Monitor 1 - Workspace 3"
option31="Monitor 1 - Workspace 4"
option41="Monitor 1 - Workspace 5"
option51="Monitor 1 - Workspace 6"
option02="Monitor 2 - Workspace 1"
option12="Monitor 2 - Workspace 2"
option22="Monitor 2 - Workspace 3"
option32="Monitor 2 - Workspace 4"
option42="Monitor 2 - Workspace 5"
option52="Monitor 2 - Workspace 6"

option0="Workspace 1"
option1="Workspace 2"
option2="Workspace 3"
option3="Workspace 4"
option4="Workspace 5"
option5="Workspace 6"

#options passed to variable


if (xrandr | grep " connected " | awk '{ print$1 }' | grep "HDMI" > /dev/null 2>&1)
then
    options="$option01\n$option11\n$option21\n$option31\n$option41\n$option51\n$option02\n$option12\n$option22\n$option32\n$option42\n$option52"
    selected="$(echo -e "$options" | rofi -dmenu -p "Workspace" -l 6 -i)"

    case $selected in
        $option01) bash -c "bspc desktop -f I";;
        $option11) bash -c "bspc desktop -f II";;
        $option21) bash -c "bspc desktop -f III";;
        $option31) bash -c "bspc desktop -f IV";;
        $option41) bash -c "bspc desktop -f V";;
        $option51) bash -c "bspc desktop -f VI";;
        $option02) bash -c "bspc desktop -f VII";;
        $option12) bash -c "bspc desktop -f VIII";;
        $option22) bash -c "bspc desktop -f IX";;
        $option32) bash -c "bspc desktop -f X";;
        $option42) bash -c "bspc desktop -f XI";;
        $option52) bash -c "bspc desktop -f XII";;
    esac
else
    options="$option0\n$option1\n$option2\n$option3\n$option4\n$option5"
    selected="$(echo -e "$options" | rofi -dmenu -p "Workspace" -l 6 -i)"

    case $selected in
        $option0) bash -c "bspc desktop -f I";;
        $option1) bash -c "bspc desktop -f II";;
        $option2) bash -c "bspc desktop -f III";;
        $option3) bash -c "bspc desktop -f IV";;
        $option4) bash -c "bspc desktop -f V";;
        $option5) bash -c "bspc desktop -f VI";;
    esac
fi
