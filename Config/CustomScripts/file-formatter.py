import os
import sys
import re

def get_unique_filename(dir_name, base_name, ext):
    new_name = re.sub(r'\s+', ' ', base_name).strip().replace(' ', '-').lower()
    new_path = os.path.join(dir_name, new_name + ext)
    i = 2
    while os.path.exists(new_path):
        new_path = os.path.join(dir_name, f"{new_name}-{i}{ext}")
        i += 1
    return new_path

def rename_files(file_paths):
    for file_path in file_paths:
        if not os.path.exists(file_path):
            print(f"File not found: {file_path}", file=sys.std.err)
            continue

        if os.path.isdir(file_path):
            print(f"{file_path} is a directory", file=sys.stderr)
            continue
        
        dir_name, original_name = os.path.split(file_path)
        base, ext = os.path.splitext(original_name)
        
        new_path = get_unique_filename(dir_name, base, ext)
        
        try:
            os.rename(file_path, new_path)
            print(f"Renamed '{file_path}' -> '{new_path}'")
        except Exception as e:
            print(f"Error renaming '{file_path}': {e}", file=sys.stderr)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python rename_files.py <file1> <file2> ...", file=sys.stderr)
        sys.exit(1)
    
    rename_files(sys.argv[1:])

