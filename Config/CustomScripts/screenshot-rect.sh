#!/bin/bash

# Create a directory for the screenshot based on the current date
mkdir -p "$(date +"/home/grey/Pictures/Screenshots/%Y/%m")"

# Take a screenshot with Flameshot
flameshot gui --path /home/grey/Pictures/Screenshots/$(date +"%Y/%m/%d-%H-%M-%S.png")

# Copy the screenshot to the clipboard
flameshot copy --clipboard

# sleep 1
# mkdir -p $(date +"/home/grey/Pictures/Screenshots/%Y/%m")
# bash -c "scrot -s -f /home/grey/Pictures/Screenshots/'%Y/%m/%d-%H-%M-%S.png' -e 'cp \$f /tmp/image.png'"
# xclip -selection clipboard -t image/png -i /tmp/image.png

