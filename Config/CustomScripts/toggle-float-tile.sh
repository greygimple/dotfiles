#!/bin/bash

# Get the current state of the monitor
current_state=$(head -n 1 ~/.config/CustomScripts/current-state.txt)

if [ "$current_state" = "floating" ]; then
    # If the current state is floating, set all windows to tiled
    # bspc query -N -n .window | while read -r wid; do
    #     bspc node "$wid" -t tiled
    # done
    
    # Change global rule
    bspc rule -a \* state=tiled
    bspc rule -a Galculator state=floating
    bspc rule -a Pavucontrol state=floating
    bspc rule -a Orage state=floating
    bspc rule -a Lxpolkit state=floating

    echo "tiled" > ~/.config/CustomScripts/current-state.txt

    # Send notification
    notify-send "Window Management" "Layout: tiled"

else
    # If the current state is not floating, set all windows to floating
    # bspc query -N -n .window | while read -r wid; do
    #     bspc node "$wid" -t floating
    # done
    
    # Change global rule
    bspc rule -a \* state=floating center=true
    bspc rule -a Brave-browser rectangle=800x1000

    echo "floating" > ~/.config/CustomScripts/current-state.txt

    # Send notification
    notify-send "Window Management" "Layout: floating"
fi

