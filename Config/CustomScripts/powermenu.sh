#!/bin/bash

#options to be displayed
option0="Shutdown"
option1="Reboot"
option2="Log Out"

#options passed to variable
options="$option0\n$option1\n$option2"

selected="$(echo -e "$options" | rofi -dmenu -p "Power Menu" -l 3 -i)"

case $selected in
    $option0) bash -c "systemctl shutdown now";;
    $option1) bash -c "systemctl reboot";;
    $option2) bash -c "bspc quit";;
esac
