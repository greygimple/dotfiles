# About The Project
This repository serves as a backup for my configuration files
Files are for my Debian 12 installation with XFCE + BSPWM

# CONFIGURATION FILES
- bspwm
- custom scripts
- doom emacs
- dunst
- kitty
- nvim
- picom
- polybar
- rofi
- sxkhd
- bashrc
- starship config
- vlc

