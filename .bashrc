# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# History size
HISTFILESIZE=2000

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Doom Path
export PATH="$HOME/.emacs.d/bin:$PATH"

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# EDITOR
export VISUAL="nvim";
export EDITOR="nvim";

# Hyprland QT Things
# export QT_QPA_PLATFORMTHEME=qt5ct

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Update bash history immediately
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

alias python3='/bin/python3'

# XDG-Open
alias open='xdg-open'

# Edit Bashrc
alias brc="nvim ~/.bashrc"

#Ignore Upper And Lowercase When TAB Completion
bind "set completion-ignore-case on"

# Reload .bashrc
alias reload="source ~/.bashrc"

# Software
alias install='sudo nala install'
alias remove='sudo nala remove'
alias update='sudo nala upgrade && brew update && brew upgrade && flatpak update'
alias search='sudo nala search'

# NVIM
alias vim="nvim"

# File Managers
alias files='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR" && ls'
# Open New File Browser
alias gfiles="thunar &"

# Get Aliases
alias acheck="alias | grep"

# Downloads Cleaner
alias cleaner-downloads="python3 ~/.config/CustomScripts/downloads-cleaner.py"

# Sort Screenshots
alias cleaner-screenshots="python3 ~/.config/CustomScripts/screenshot-sorter.py"

# File Formatter
alias file-formatter="python3 ~/.config/CustomScripts/file-formatter.py"

# UWaterloo SSH
alias uwssh='ssh -Y gjbgimpl@linux.student.cs.uwaterloo.ca'
alias uwm='sshfs gjbgimpl@linux.student.cs.uwaterloo.ca:/u5/gjbgimpl ~/UWSSH'
alias uwum='umount ~/UWSSH'
alias uwreload='umount ~/UWSSH && sleep .25 && sshfs gjbgimpl@linux.student.cs.uwaterloo.ca:/u5/gjbgimpl ~/UWSSH'
uw_cp() {
  scp -r "$1" gjbgimpl@linux.student.cs.uwaterloo.ca:.
}
alias uwcp='uw_cp'

# youtube-dl
alias ytdlv='yt-dlp'
alias ytdla='yt-dlp -x --audio-format mp3'

# Python Shell
alias py='python3'

# Changing "ls" to "eza"
# All files
alias la="eza -alh --color=always --group-directories-first --icons --git --classify --time-style=long-iso"
# All files recursive
alias lar="eza -lh --recurse --color=always --group-directories-first --icons --ignore-glob='.git' --git --classify --time-style=long-iso"
# Regular files
alias ls="eza -lh --color=always --group-directories-first --icons --git --classify --time-style=long-iso"
# Regular files recursive
alias lsr="eza -lh --recurse --color=always --group-directories-first --icons --ignore-glob='.git' --git --time-style=long-iso"

# Better Less
alias less='less -FX'

# Confirm Remove and Move
alias rm="rm -i"
alias mv="mv -i"

# Terminal Fun
alias clock='tty-clock -s -S -c -C 6 -t -f %A-%d-%B-%Y'
alias pipes='pipes.sh -p 15 -s 15 -K -R -t 0'
alias dots='pipes.sh -p 15 -s 15 -R -t 6'
alias cmatrix='cmatrix -B -o -u 8'
alias cbonsai='cbonsai -S -i -m "I am the Lorax,
I speak for the trees.
Chop another down,
Ill break your fucking knees."'

# Neofetch
alias nf='neofetch'

alias cal="if [ -t 1 ] ; then ncal -b ; else /usr/bin/cal ; fi"

# Internet
alias internet='nmtui'

# Emacs
alias emacs="emacsclient -c -a 'emacs'"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"

# Git
alias addup="git add -U && git status"
alias addall="git add . && git status"
alias add="git add"
alias unadd="git rm --cached -r"
alias branch="git branch"
alias checkout="git checkout"
alias clone="git clone"
alias commit="git commit -m"
alias fetch="git fetch"
alias merge="git merge"
alias push="git push origin"
alias pull="git pull origin"
alias restore="git restore"
alias stat="git status"

# DotFiles
alias updatedots="~/Dotfiles/fetch-configs.py"

# Intelij
# alias intelij="~/Programs/idea-IC-222.3739.54/bin/idea.sh"

# stream phone screen
# alias phone-screen='adb connect 192.168.2.13:5555 && scrcpy -m 1024'

# Open New Terminal
alias term="nohup kitty &>/dev/null &"

# g++20 shortcuts
alias gppm="g++ -std=c++20 -fmodules-ts -Wall -g"
alias gpph="g++ -std=c++20 -fmodules-ts -c -x c++-system-header"
alias gppi="g++ -std=c++20 -Wall -g"

# Navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

alias updatedb="sudo updatedb"

# FZF
alias cdf='cd "$(find . -path ./.git -prune -o -print | fzf --preview="if [ -d '\''{}'\'' ]; then ls -la '\''{}'\''; else batcat '\''{}'\''; fi" | xargs -r -I {} dirname "{}")"'
alias vf='selected_file=$(find . -path ./.git -prune -o -print
| fzf --preview="batcat {}") && [ -n "$selected_file" ] && nvim "$selected_file"'
alias of='selected_file=$(find . -path ./.git -prune -o -print | fzf --preview="batcat {}") && [ -n "$selected_file" ] && xdg-open "$selected_file"'
alias cpf='selected_file=$(find . -path ./.git -prune -o -print | fzf --preview="batcat {}") && [ -n "$selected_file" ] && echo "$selected_file" | xclip -selection clipboard'
alias rmf='selected_file=$(find . -path ./.git -prune -o -print | fzf --preview="batcat {}") && [ -n "$selected_file" ] && rm -i "$selected_file"'
alias h='history | fzf --no-preview --tac --no-sort | awk "{print \$2}" | xargs -I % sh -c "%"'
alias rh='eval $(history | fzf --no-preview | awk "{print \$2}")'
alias ff="find . -path ./.git -prune -o -print| fzf --preview='batcat {}'"

# Cat -> Bat
alias bat='batcat'

# Line Counter
alias checklines='function _checklines() { IFS="," read -ra EXT <<< "$1"; find . -type f \( $(printf -- "-name *.%s -o " "${EXT[@]}") -false \) -exec wc -l {} + | awk '\''{total += $1} END {print total}'\''; }; _checklines'


# neofetch
# rxfetch
# pridefetch -f trans


# fnm
export PATH="/home/grey/.local/share/fnm:$PATH"
eval "$(fnm env --multi 2> /dev/null)"
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

unset rc

eval "$(starship init bash)"

