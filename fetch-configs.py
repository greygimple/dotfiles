#!/usr/bin/env python3
import os

homeConfigs = [".bashrc", ".xbindkeysrc"]
dotConfigs = ["bspwm", "CustomScripts", "dunst", "kitty", "picom", "polybar", "rofi", "sxhkd", "starship.toml", "vlc"]

for config in homeConfigs:
    command = "cp -r ~/" + config + " ~/Dotfiles/"
    os.system(command)

for config in dotConfigs:
    command = "cp -r ~/.config/" + config + " ~/Dotfiles/Config/"
    os.system(command)
